import socket

class contentPutApp:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.resources = {}

    def analyze(self, request):
        lines = request.split(b'\r\n')
        method, path, _ = lines[0].decode('utf-8').split(' ')
        body_start = request.find(b'\r\n\r\n') + 4  # Encontrar el inicio del cuerpo de la solicitud
        body = request[body_start:].decode('utf-8')
        return {"method": method, "path": path, "body": body}

    def compute(self, analysis):
        method = analysis["method"]
        path = analysis["path"]

        if method == "PUT":
            if path in self.resources:
                # Actualizar el contenido con el cuerpo de la solicitud PUT
                self.resources[path] = analysis["body"]
                code = "200 OK"
                html = f"<html><body>Contenido de {path} actualizado con éxito</body></html>"
            else:
                code = "404 Not Found"
                html = "<html><body>Recurso no encontrado</body></html>"
        else:
            code = "405 Method Not Allowed"
            html = "<html><body>Método no permitido</body></html>"

        return code, html

    def run(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((self.host, self.port))
            s.listen()

            print(f"Servidor escuchando en {self.host}:{self.port}")

            while True:
                conn, addr = s.accept()
                with conn:
                    print(f"Conexión establecida desde {addr}")
                    data = conn.recv(1024)
                    if not data:
                        break

                    # Analizar la solicitud
                    analysis = self.analyze(data)

                    # Calcular la respuesta
                    code, html = self.compute(analysis)

                    # Enviar la respuesta al cliente
                    response = f"HTTP/1.1 {code}\r\n\r\n{html}"
                    conn.sendall(response.encode("utf-8"))

if __name__ == "__main__":
    app = contentPutApp("localhost", 8080)
    app.run()
